package pocversion.acaro.com.bookswap;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ViewListActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    ListView otherSwapListView;
    private ArrayAdapter<Book> bookListAdapter;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("bookswapdb").child("swaplist");

    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    final String uid = user.getUid();

    public static String TAG = "ViewListActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        <--  Source: https://developer.android.com/training/implementing-navigation/nav-drawer.html Accessed: February 2017<date>
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
//        -->

        otherSwapListView = (ListView) findViewById(R.id.otherSwapListView);

        Intent intent = getIntent();
        Bundle data = intent.getExtras();

        final String requesterID = data.getString("requesterID");

        final ArrayList<Book> bookList = new ArrayList<>();
        final ArrayList<String> bookTitle = new ArrayList<>();

        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {

                    if (messageSnapshot.getKey().equals("userId")) {
                        if (messageSnapshot.getValue().toString().equals((requesterID))) {

                            for (DataSnapshot newMessageSnapshot : dataSnapshot.getChildren()) {

                                if (newMessageSnapshot.getKey().equals("title")) {

                                    Book book = dataSnapshot.getValue(Book.class);
                                    bookList.add(book);
                                    bookTitle.add(newMessageSnapshot.getValue().toString());
                                }
                            }
                        }
                    }

                }

                otherSwapListView = (ListView) findViewById(R.id.otherSwapListView);

                bookListAdapter = new ArrayAdapter(ViewListActivity.this, R.layout.list_item, bookTitle);
                otherSwapListView.setAdapter(bookListAdapter);

                otherSwapListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ViewListActivity.this);

                        builder.setTitle("Book Swap");

                        builder.setMessage("Would you like to swap this book?");

                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int id) {
                                Toast.makeText(getApplicationContext(), "Swapped Book", Toast.LENGTH_SHORT);

                                //create chat ID
                                Contact newContact = new Contact(uid, requesterID);

                                String chatKey = uid.concat(requesterID);

                                Map<String, Object> contactValues = newContact.toMap();

                                createContact(contactValues, chatKey);

                                //go to messaging
                                Intent intent = new Intent(ViewListActivity.this, MessagesMenuActivity.class);
                                startActivity(intent);

                            }

                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                Toast.makeText(ViewListActivity.this, "Deleting book from the database... ", Toast.LENGTH_SHORT).show();

                                final Book item = bookList.get(i);

                                final String title = item.getTitle();

                                Query query = database.getReference().child("bookswapdb").child("pendingrequestlist").orderByChild("title").equalTo(title);

                                query.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        for (DataSnapshot snapshot: dataSnapshot.getChildren()) {

                                            snapshot.getRef().removeValue();

                                            for (int i =0; i > bookList.size(); i++) {
                                                if (bookList.get(i).getTitle().equals(title)) {
                                                    bookList.remove(i);
                                                }
                                            }
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                        Log.e(TAG, "onCancelled", databaseError.toException());
                                    }
                                });

                                dialog.cancel();
                            }
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void createContact (Map<String, Object> contactValues, String chatKey) {
        DatabaseReference newRef = database.getReference("bookswapdb").child("contacts");

        Map<String, Object> childUpdates = new HashMap<>();

        childUpdates.put(chatKey, contactValues);

        newRef.updateChildren(childUpdates);
    }

//    <--  Source: https://developer.android.com/training/implementing-navigation/nav-drawer.html Accessed: February 2017<date>
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;

        if (id == R.id.nav_home) {
            intent = new Intent(ViewListActivity.this, HomeActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_profile) {
            intent = new Intent(ViewListActivity.this, ProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_messages) {
            intent = new Intent(ViewListActivity.this, MessagesMenuActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
//    -->
}
