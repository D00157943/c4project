package pocversion.acaro.com.bookswap;

/**
 * Created by acaro on 28/03/2017.
 */

public class Message {

    private String messageText;
    private String messageUser;
    private String chatID;

    public Message(String messageText, String messageUser, String chatID) {
        this.messageText = messageText;
        this.messageUser = messageUser;
        this.chatID = chatID;
    }

    public Message(String messageText, String messageUser) {
        this.messageText = messageText;
        this.messageUser = messageUser;
    }

    public Message(){

    }

    public String getMessageChatID() {
        return chatID;
    }

    public void setMessageChatID(String chatID) {
        this.chatID = chatID;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageUser() {
        return messageUser;
    }

    public void setMessageUser(String messageUser) {
        this.messageUser = messageUser;
    }
}
