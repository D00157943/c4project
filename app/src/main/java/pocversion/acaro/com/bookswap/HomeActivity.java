package pocversion.acaro.com.bookswap;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    protected static final String TAG = "HomeActivity";

    private boolean ifRequested;
    private String bookKey;

    ListView displayBooksListView;
    private ArrayAdapter<Book> bookListAdapter;

    GeofenceStore geoStore;

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;

    /**
     * Represents a geographical location.
     */
    protected Location mLastLocation;

    private static final int REQUEST_LOCATION = 2;

    //Connect to Firebase Auth
    FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    //Connect to Database
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("bookswapdb");

    //Get user data from Firebase
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    final String uid = user.getUid();

    //Hold latlng values
    private double currentLatitude;
    private double currentLongitude;
    private float radius = 200;

    private double savedLatitude;
    private double savedLongitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample);
        buildGoogleApiClient();

//      <--  Source: https://developer.android.com/training/implementing-navigation/nav-drawer.html Accessed: February 2017
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
//        -->

        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };

        Toast.makeText(getApplicationContext(), "Signed in", Toast.LENGTH_SHORT).show();

        //myRef gets database reference and adds table to search through
        myRef = myRef.child("userlocation");

        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                if (!dataSnapshot.getKey().equals(uid)) {

                    //get user's ID stored as key and check if ID matches current logged in user
                    String otherID = dataSnapshot.getKey();
                    //loop through children of the uid key
                    for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {

                        if (messageSnapshot.getKey().equals("latitude")) {
                            savedLatitude = Double.parseDouble(messageSnapshot.getValue().toString());
                        }

                        if (messageSnapshot.getKey().toString().equals("longitude")) {
                            savedLongitude = Double.parseDouble(messageSnapshot.getValue().toString());
                        }

                        UserLocation userLocations = new UserLocation(savedLatitude, savedLongitude);

                        ArrayList<UserLocation> coordinates = new ArrayList<UserLocation>();
                        coordinates.add(userLocations);

                        HashMap<String, ArrayList<UserLocation>> map = new HashMap<String, ArrayList<UserLocation>>();
                        map.put(otherID, coordinates);

                        find(otherID, map);

                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    //find books in database belonging to other users and within geofence
    private void find(final String otherID, final Map<String, ArrayList<UserLocation>> map) {

        //look through userlocation table
        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {
                    //get user ID key

                    if (messageSnapshot.getKey().toString().equals(uid)) {

                        //Search through other table
                        final DatabaseReference newRef = database.getReference("bookswapdb").child("swaplist");

                        final ArrayList<Book> bookList = new ArrayList<>();
                        final ArrayList<String> bookTitle = new ArrayList<>();
                        final ArrayList<String> bookKeys = new ArrayList<>();

                        if (bookList.isEmpty()) {
                            newRef.addChildEventListener(new ChildEventListener() {
                                @Override
                                public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                                    for (DataSnapshot secondMessageSnapshot : dataSnapshot.getChildren()) {
                                        //if the key matches the user ID

                                        if (secondMessageSnapshot.getKey().equals("userId")) {
                                            //take userId value that matches
                                            String otherUserID = secondMessageSnapshot.getValue().toString();

                                            //if it does not equal current user
                                            if (!otherUserID.equals(uid)) {
                                                //if within location - last known location
                                                bookKey = dataSnapshot.getKey().toString();

                                                for (Map.Entry<String, ArrayList<UserLocation>> entry : map.entrySet()) {

                                                    if (!map.containsKey(uid)) {

                                                        for (UserLocation l : map.get(otherID)) {

                                                            for (DataSnapshot thirdSnapshot : dataSnapshot.getChildren()) {
                                                                if (thirdSnapshot.getKey().equals("ifRequested")) {

                                                                    ifRequested = Boolean.parseBoolean(thirdSnapshot.getValue().toString());

                                                                    if (ifRequested == false) {
                                                                        for (DataSnapshot fourthSnapshot : dataSnapshot.getChildren()) {

                                                                            boolean check = geoStore.checkInside(l.getLatitude(), l.getLongitude());
                                                                            if (check == true && otherID.equals(otherUserID)) {

                                                                                if (fourthSnapshot.getKey().equals("title")) {

                                                                                    Book book = dataSnapshot.getValue(Book.class);
                                                                                    bookList.add(book);

                                                                                    bookTitle.add(fourthSnapshot.getValue().toString());
                                                                                    bookKeys.add(bookKey);
                                                                                }
                                                                            } else {
                                                                                System.out.println("not matching");
                                                                            }

                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            } else {
                                                System.out.println("matches uid");
                                            }
                                        }
                                    }

                                    displayBooksListView = (ListView) findViewById(R.id.displayBooksListView);

                                    bookListAdapter = new ArrayAdapter(HomeActivity.this, R.layout.list_item, bookTitle);
                                    displayBooksListView.setAdapter(bookListAdapter);

                                    displayBooksListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
//                                            <-- AlertDialogue  Source: <source> Accessed: <date>
                                            AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);

                                            builder.setTitle("Swap Book?");

                                            builder.setMessage("Would you like to swap this book?");

                                            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                                                public void onClick(DialogInterface dialog, int id) {
                                                    //send to pending requests of other user list
                                                    CreatePendingRequest newRequest = new CreatePendingRequest(bookTitle.get(i), otherID, bookKey, uid);
                                                    Map<String, Object> request = newRequest.toMap();
                                                    makeRequest(request, bookKeys.get(i));

                                                    newRef.addChildEventListener(new ChildEventListener() {
                                                        @Override
                                                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                                            String key = dataSnapshot.getKey();

                                                            if (bookKeys.get(i).equals(key)) {
                                                                ifRequested = true;

                                                                for (DataSnapshot capture : dataSnapshot.getChildren()) {

                                                                    if (capture.getKey().equals("ifRequested")) {

                                                                        Toast.makeText(getApplicationContext(), "Swap Requested", Toast.LENGTH_SHORT).show();

                                                                        Map<String, Object> childUpdates = new HashMap<>();
//
                                                                        childUpdates.put("/" + key + "/" + capture.getKey() + "/", ifRequested);
//
                                                                        newRef.updateChildren(childUpdates);
                                                                    }
                                                                }


                                                            }
                                                        }

                                                        @Override
                                                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                                        }

                                                        @Override
                                                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                                                        }

                                                        @Override
                                                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                                        }

                                                        @Override
                                                        public void onCancelled(DatabaseError databaseError) {

                                                        }
                                                    });
                                                }

                                            });
                                            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });
                                            AlertDialog dialog = builder.create();
                                            dialog.show();
                                        }
                                    });
                                }

                                @Override
                                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                }

                                @Override
                                public void onChildRemoved(DataSnapshot dataSnapshot) {

                                }

                                @Override
                                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }


                    }
                }
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


//    <--  Source: https://developer.android.com/training/implementing-navigation/nav-drawer.html Accessed: February 2017
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;

        if (id == R.id.nav_home) {
            intent = new Intent(HomeActivity.this, HomeActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_profile) {
            intent = new Intent(HomeActivity.this, ProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_messages) {
            intent = new Intent(HomeActivity.this, MessagesMenuActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
//  -->
//  <--  Source: https://developer.android.com/training/location/retrieve-current.html Accessed: March 2017
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("onStart");
        mGoogleApiClient.connect();

        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            System.out.println("onStop");
            mGoogleApiClient.disconnect();
        }

        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

    /**
     * Runs when a GoogleApiClient object successfully connects.
     */


    @Override
    public void onConnected(Bundle connectionHint) {
        // Provides a simple way of getting a device's location and is well suited for
        // applications that do not require a fine-grained location and that do not need location
        // updates. Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.

        //  <--  Source: https://developer.android.com/training/location/retrieve-current.html Accessed: March 2017
        if (Build.VERSION.SDK_INT >= 25 &&
                ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_LOCATION);
        } else {
            // permission has been granted, continue as usual
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            if (mLastLocation != null) {
                UserLocation getLocation = new UserLocation(mLastLocation.getLatitude(), mLastLocation.getLongitude());

                currentLatitude = mLastLocation.getLatitude();
                currentLongitude = mLastLocation.getLongitude();

                geoStore = new GeofenceStore(currentLatitude, currentLongitude, radius);

                Map<String, Object> locationValues = getLocation.toMap();

                addLocationToDatabase(locationValues, myRef);

            } else {
                Toast.makeText(this, R.string.no_location_detected, Toast.LENGTH_LONG).show();
            }

        }

    }
//  -->

    private void addLocationToDatabase(Map<String, Object> locationValues, DatabaseReference myRef) {
        Map<String, Object> childUpdates = new HashMap<>();

        childUpdates.put(uid, locationValues);

        myRef.updateChildren(childUpdates);
    }

    private void makeRequest (Map<String, Object> request, String bookKey) {
        DatabaseReference newReference = database.getReference("bookswapdb").child("pendingrequestlist");

        Map<String, Object> childUpdates = new HashMap<>();

        childUpdates.put(bookKey, request);

        newReference.updateChildren(childUpdates);
    }

    //  <--  Source: https://developer.android.com/training/location/retrieve-current.html Accessed: March 2017
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions,
                                           int[] grantResults) {
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.length == 1
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // We can now safely use the API we requested access to
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                        ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                Location myLocation =
                        LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            } else {
                // Permission was denied or request was cancelled
            }
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // Refer to the javadoc for ConnectionResult to see what error codes might be returned in
        // onConnectionFailed.
        System.out.println("Fail");
        Log.i(TAG, "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());
    }


    @Override
    public void onConnectionSuspended(int cause) {
        // The connection to Google Play services was lost for some reason. We call connect() to
        // attempt to re-establish the connection.
        Log.i(TAG, "Connection suspended");
        mGoogleApiClient.connect();
    }
//    -->
}
