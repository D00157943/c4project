package pocversion.acaro.com.bookswap;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by acaro on 21/02/2017.
 */

public class Page2AddBookActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private static String TAG = "Page2AddBookActivity";

    Spinner addBookGenreSpinner;
    EditText addBookDesc;
    EditText addBookAddedInfo;
    Button saveBook;
    Button cancelBook;
    ImageButton imageButtonBack;

    FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page2_add_book);

        //initilise toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Navigation Drawer
//        <--  Source: https://developer.android.com/training/implementing-navigation/nav-drawer.html Accessed: February 2017<date>
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
//        -->

        //hide sofe keyboard onCreate activity
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //Firebase Authentication
        mAuth = FirebaseAuth.getInstance();

        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // ...
            }
        };

        //getCurrentUser
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final String uid = user.getUid();

        //Firebase Database
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myRef = database.getReference("bookswapdb").child("swaplist");

        //Initialise book genre spinner
        addBookGenreSpinner = (Spinner) findViewById(R.id.addBookGenreSpinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.genre__spinner_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        addBookGenreSpinner.setAdapter(adapter);

        addBookGenreSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int pos, long id) {

                String item = parent.getItemAtPosition(pos).toString();
                addBookGenreSpinner.setSelection(pos);
                Toast.makeText(Page2AddBookActivity.this,
                        "OnClickListener : " +
                                "\nSpinner 1 : "+ String.valueOf(addBookGenreSpinner.getSelectedItem()),
                        Toast.LENGTH_SHORT).show();
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }

        });

        //Get values passed from previous activity
        Intent intent = getIntent();
        Bundle data = intent.getBundleExtra("bookData");

        final String bookTitle = data.getString("bookTitle");
        final String bookAuthor = data.getString("bookAuthor");
        final String bookPublisher = data.getString("bookPublisher");
        final String bookPubDate = data.getString("bookPubDate");
        final String bookQuality = data.getString("bookQuality");

        saveBook = (Button) findViewById(R.id.addBookSaveButton);
        cancelBook = (Button) findViewById(R.id.addBookCancelButton);
        imageButtonBack = (ImageButton) findViewById(R.id.imageButtonBack);

        saveBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addBookDesc = (EditText) findViewById(R.id.addBookDescEditText);
                addBookAddedInfo = (EditText) findViewById(R.id.addBookInfoEditText);

                final String bookDesc = addBookDesc.getText().toString().trim();
                final String bookAddedInfo = addBookAddedInfo.getText().toString().trim();
                final String bookGenre = addBookGenreSpinner.getSelectedItem().toString().trim();
                final boolean ifRequested = false;

                Book newBook = new Book(uid, bookTitle, bookAuthor, bookPublisher, bookPubDate, bookQuality, bookGenre, bookDesc, bookAddedInfo, ifRequested);
                Map<String, Object> bookValues = newBook.toMap();

                Toast.makeText(Page2AddBookActivity.this,
                        "User ID: " + uid + "Title: " + bookTitle + " Author: " + bookAuthor + " Publisher: " + bookPublisher + " Publication Date: " + bookPubDate + " Quality: " + bookQuality + " Desc: " + bookDesc + " Added Notes: " + bookAddedInfo,
                        Toast.LENGTH_SHORT).show();

                addBookToDatabase(bookValues, myRef);
            }
        });

        cancelBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(Page2AddBookActivity.this,
                        "Cancel Button",
                        Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(Page2AddBookActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });

        imageButtonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Page2AddBookActivity.this, Page1AddBookActivity.class);
                startActivity(intent);
            }
        });
    }

    private void addBookToDatabase (Map<String, Object> bookValues, DatabaseReference myRef) {
            String key = myRef.child("bookswapdb").child("swapbooklist").push().getKey();

            Map<String, Object> childUpdates = new HashMap<>();

            childUpdates.put(key, bookValues);

            myRef.updateChildren(childUpdates);

            Intent intent = new Intent(Page2AddBookActivity.this, ProfileActivity.class);
            startActivity(intent);
    }

    @Override
    public void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }

//    <--  Source: https://developer.android.com/training/implementing-navigation/nav-drawer.html Accessed: February 2017<date>
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;

        if (id == R.id.nav_home) {
            intent = new Intent(Page2AddBookActivity.this, HomeActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_profile) {
            intent = new Intent(Page2AddBookActivity.this, ProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_messages) {
            intent = new Intent(Page2AddBookActivity.this, MessagesMenuActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
//    -->
}
