package pocversion.acaro.com.bookswap;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MessagesMenuActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    ListView contactListView;
    ArrayAdapter<Contact> contactArrayAdapter;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("bookswapdb").child("contacts");

    String currentUser;
    String requesterID;
    String getChatID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages_menu);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        <--  Source: https://developer.android.com/training/implementing-navigation/nav-drawer.html Accessed: February 2017<date>
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
//        -->

        final ArrayList<String> contacts = new ArrayList<>();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    for (DataSnapshot snapshotVal : snapshot.getChildren()) {

                        if (snapshotVal.getKey().equals("uid")) {

                            currentUser = snapshotVal.getValue().toString();

                            for (DataSnapshot findRequester : snapshot.getChildren()) {

                                if (findRequester.getKey().equals("requesterID")) {

                                    requesterID = findRequester.getValue().toString();

                                    for (DataSnapshot findID : snapshot.getChildren()) {

                                        if (findID.getKey().equals("chatID")) {

                                            getChatID = findID.getValue().toString();

                                            contacts.add(getChatID);
                                        }
                                    }
                                }
                            }
                        }

                    }
                }

                contactListView = (ListView) findViewById(R.id.listViewMessagesMenu);

                contactArrayAdapter = new ArrayAdapter(MessagesMenuActivity.this, R.layout.list_item, contacts);
                contactListView.setAdapter(contactArrayAdapter);

                contactListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                        Intent intent = new Intent(MessagesMenuActivity.this, MessagesActivity.class);
                        Bundle bundle = new Bundle();
                        bundle.putString("chatID", getChatID);
                        intent.putExtra("data", bundle);
                        startActivity(intent);
                    }
                });

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

//    <--  Source: https://developer.android.com/training/implementing-navigation/nav-drawer.html Accessed: February 2017<date>
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;

        if (id == R.id.nav_home) {
            intent = new Intent(MessagesMenuActivity.this, HomeActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_profile) {
            intent = new Intent(MessagesMenuActivity.this, ProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_messages) {
            intent = new Intent(MessagesMenuActivity.this, MessagesMenuActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
//    -->
}
