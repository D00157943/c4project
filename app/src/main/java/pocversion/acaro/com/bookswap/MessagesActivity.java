package pocversion.acaro.com.bookswap;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class MessagesActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    //Authentication objects
    private FirebaseAuth mAuth;

    //Database connection
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("bookswapdb").child("messages");

    ListView listOfMessages;
    ArrayAdapter<Message> messagesAdapter;

    String messageText;
    String messageUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        <--  Source: https://developer.android.com/training/implementing-navigation/nav-drawer.html Accessed: February 2017<date>
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
//        -->

        Intent intent = getIntent();
        Bundle getData = intent.getBundleExtra("data");
        final String chatID = getData.getString("chatID");

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        //initialise to track if user signs in or out
        mAuth = FirebaseAuth.getInstance();

        listOfMessages = (ListView) findViewById(R.id.list_of_messages);


        Button fab = (Button) findViewById(R.id.send_message_fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText msg = (EditText) findViewById(R.id.input);
                myRef.push().setValue(new Message(msg.getText().toString(),
                        mAuth.getCurrentUser().getEmail(), chatID));

                msg.setText("");
            }
        });

        loadMessages(chatID);
    }

    //pull messages from storage
    private void loadMessages (final String chatID) {

        final ArrayList<String> newMessage = new ArrayList<>();

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot snapshot: dataSnapshot.getChildren()) {

                    for (DataSnapshot getID: snapshot.getChildren()) {

                        if (getID.getValue().equals(chatID)) {

                            for (DataSnapshot getMessage : snapshot.getChildren()) {

                                if (getMessage.getKey().equals("messageText")) {

                                    messageText = getMessage.getValue().toString();

                                    for (DataSnapshot getUser : snapshot.getChildren()) {
                                        if (getUser.getKey().equals("messageUser")) {

                                            messageUser = getUser.getValue().toString();

                                            String msg = "From: " + messageUser + "\n" + messageText;

                                            if (!newMessage.contains(msg)) {
                                                newMessage.add(msg);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                messagesAdapter = new ArrayAdapter(MessagesActivity.this, R.layout.message_layout, newMessage);
                listOfMessages.setAdapter(messagesAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


//   <--  Source: https://developer.android.com/training/implementing-navigation/nav-drawer.html Accessed: February 2017<date>
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;

        if (id == R.id.nav_home) {
            intent = new Intent(MessagesActivity.this, HomeActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_profile) {
            intent = new Intent(MessagesActivity.this, ProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_messages) {
            intent = new Intent(MessagesActivity.this, MessagesMenuActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
//    -->
}
