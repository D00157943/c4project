package pocversion.acaro.com.bookswap;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by acaro on 24/04/2017.
 */

public class Contact {

    private int chatID;
    private String uid;
    private String requesterID;
    private static int count = 0;

    public Contact () {

    }

    public Contact (String uid, String requesterID) {
        this.uid = uid;
        this.requesterID = requesterID;

        chatID = ++count;
    }

    public int getChatID() {
        return chatID;
    }

    public void setChatID(int chatID) {
        this.chatID = chatID;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getRequesterID() {
        return requesterID;
    }

    public void setRequesterID(String requesterID) {
        this.requesterID = requesterID;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        Contact.count = count;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("chatID", chatID);
        result.put("uid", uid);
        result.put("requesterID", requesterID);

        return result;
    }
}
