package pocversion.acaro.com.bookswap;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by acaro on 15/03/2017.
 */

public class UserLocation {
    private double latitude;
    private double longitude;
    private String uid;

    public UserLocation () {

    }

    public UserLocation (double latitude, double longitude) {
//        this.uid = uid;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getUid() {
        return uid;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
//        result.put("userId", uid);
        result.put("latitude", latitude);
        result.put("longitude", longitude);

        return result;
    }
}
