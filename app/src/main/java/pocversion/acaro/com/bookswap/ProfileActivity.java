package pocversion.acaro.com.bookswap;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ProfileActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private ProfileActivity.SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    public static String TAG = "ProfileActivity";

    static FirebaseDatabase database = FirebaseDatabase.getInstance();
    static DatabaseReference myRef = database.getReference("bookswapdb").child("swaplist");

    static ListView homeListView;
    static private ArrayAdapter<Book> bookListAdapter;

    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAuth = FirebaseAuth.getInstance();

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new ProfileActivity.SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        TabLayout tabs = (TabLayout) findViewById(R.id.tabs);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        tabs.setupWithViewPager(mViewPager);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ProfileActivity.this, Page1AddBookActivity.class);
                startActivity(intent);
            }
        });

//        <--  Source: https://developer.android.com/training/implementing-navigation/nav-drawer.html Accessed: February 2017
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;

        if (id == R.id.nav_home) {
            intent = new Intent(ProfileActivity.this, HomeActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_profile) {
            intent = new Intent(ProfileActivity.this, ProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_messages) {
            intent = new Intent(ProfileActivity.this, MessagesMenuActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_logout) {
            Toast.makeText(getApplicationContext(), "Sign Out Button", Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
//    -->


//    <Source: http://www.voidynullness.net/blog/2015/08/16/android-tablayout-design-support-library-tutorial/ Accessed: February 2017
//    <Source: http://www.truiton.com/2015/06/android-tabs-example-fragments-viewpager/ Accessed: February 2017
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        View rootView;

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        final String uid = user.getUid();

        ListView pendingListView;
        private ArrayAdapter<CreatePendingRequest> requestListAdapter;

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static ProfileActivity.PlaceholderFragment newInstance(int sectionNumber) {
            ProfileActivity.PlaceholderFragment fragment = new ProfileActivity.PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            switch (getArguments().getInt(ARG_SECTION_NUMBER)) {
                case 1: {
                    rootView = inflater.inflate(R.layout.content_profile, container, false);

                    final ArrayList<Book> bookList = new ArrayList<>();
                    final ArrayList<String> bookTitle = new ArrayList<>();

                    myRef.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                            final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                            for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {

                                if (messageSnapshot.getKey().toString().equals("userId")) {

                                    if (messageSnapshot.getValue().toString().equals((user.getUid()))) {

                                        for (DataSnapshot newMessageSnapshot : dataSnapshot.getChildren()) {

                                            if (newMessageSnapshot.getKey().equals("title")) {

                                                Book book = dataSnapshot.getValue(Book.class);
                                                bookList.add(book);
                                                bookTitle.add(newMessageSnapshot.getValue().toString());
                                            }
                                        }
                                    }
                                    System.out.println("Add to list");

                                }
                            }

                            homeListView = (ListView) rootView.findViewById(R.id.homeListView);


                            bookListAdapter = new ArrayAdapter(rootView.getContext(), R.layout.list_item, bookTitle);
                            homeListView.setAdapter(bookListAdapter);


                            homeListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                                @Override
                                public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int position, long l) {
                                    System.out.println("Hi OnLongClick");

                                    AlertDialog.Builder builder = new AlertDialog.Builder(rootView.getContext());

                                    builder.setTitle("Remove Book From List?");

                                    builder.setMessage("Are you sure you want to delete this book?");

                                    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                                        public void onClick(DialogInterface dialog, int id) {

                                            Toast.makeText(rootView.getContext(), "Deleting book from the database... ", Toast.LENGTH_SHORT).show();

                                            final Book item = bookList.get(position);

                                            final String title = item.getTitle();

                                            Query query = database.getReference().child("bookswapdb").child("swaplist").orderByChild("title").equalTo(title);

                                            query.addListenerForSingleValueEvent(new ValueEventListener() {
                                                @Override
                                                public void onDataChange(DataSnapshot dataSnapshot) {
                                                    for (DataSnapshot snapshot: dataSnapshot.getChildren()) {
                                                        snapshot.getRef().removeValue();

                                                        for (int i =0; i > bookList.size(); i++) {
                                                            if (bookList.get(i).getTitle().equals(title)) {
                                                                bookList.remove(i);
                                                            }
                                                        }
                                                    }
                                                }

                                                @Override
                                                public void onCancelled(DatabaseError databaseError) {
                                                    Log.e(TAG, "onCancelled", databaseError.toException());
                                                }
                                            });
                                        }

                                    });
                                    builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                                    AlertDialog dialog = builder.create();
                                    dialog.show();
                                    return true;
                                }
                            });

                        }


                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {
                            String commentKey = dataSnapshot.getKey();

                            for (DataSnapshot messageSnapshot : dataSnapshot.getChildren()) {

                                if (messageSnapshot.getKey().equals("title")) {

                                    bookList.remove(messageSnapshot.getValue());
                                }
                            }
                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }

                    });

                    break;
                }
                case 2: {
                    rootView = inflater.inflate(R.layout.content_pending_req, container, false);

                    final DatabaseReference mRef = database.getReference("bookswapdb").child("pendingrequestlist");

                    final ArrayList<String> bookRequestsTitle = new ArrayList<>();

                    //list view
                    mRef.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                                if (snapshot.getKey().equals("ownerID")) {

                                    final String ownerId = snapshot.getValue().toString();

                                    if (ownerId.equals(uid)) {

                                        for (DataSnapshot newSnapshot : dataSnapshot.getChildren()) {

                                            if (newSnapshot.getKey().equals("title")) {

                                                bookRequestsTitle.add(newSnapshot.getValue().toString());

                                            }
                                        }
                                    }

                                }
                            }

                            pendingListView = (ListView) rootView.findViewById(R.id.pendingListView);

                            requestListAdapter = new ArrayAdapter(rootView.getContext(), R.layout.list_item, bookRequestsTitle);
                            pendingListView.setAdapter(requestListAdapter);

                            pendingListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                                    mRef.addChildEventListener(new ChildEventListener() {
                                        @Override
                                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                            for (DataSnapshot snap : dataSnapshot.getChildren()) {

                                                if (snap.getKey().equals("title")) {

                                                    String title = snap.getValue().toString();

                                                    if (title.equals(bookRequestsTitle.get(i))) {

                                                        for (DataSnapshot shot : dataSnapshot.getChildren()) {

                                                            if (shot.getKey().equals("requesterID")) {

                                                                String requesterID = shot.getValue().toString();

                                                                //go to new activity
                                                                Intent intent = new Intent (rootView.getContext(), ViewListActivity.class);
                                                                intent.putExtra("requesterID", requesterID);
                                                                startActivity(intent);
                                                            }
                                                        }
                                                    }

                                                }
                                            }

                                        }

                                        @Override
                                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                        }

                                        @Override
                                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                                        }

                                        @Override
                                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                                }
                            });
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    break;
                }

            }
            return rootView;
        }
    }



    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            return ProfileActivity.PlaceholderFragment.newInstance(position + 1);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "PROFILE";
                case 1:
                    return "SWAP REQUESTS";
            }
            return null;
        }
    }
}
