package pocversion.acaro.com.bookswap;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by acaro on 23/02/2017.
 */

public class Book {

    private String userId;
    private String title;
    private String author;
    private String publisher;
    private String pubDate;
    private String quality;
    private String genre;
    private String desc;
    private String addedNotes;
    private boolean ifRequested;

    public Book () {

    }

    public Book (String title, String author, String publisher, String pubDate, String quality, String genre, String desc, String addedNotes) {
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.pubDate = pubDate;
        this.quality = quality;
        this.genre = genre;
        this.desc = desc;
        this.addedNotes = addedNotes;
    }

    public Book (String userId, String title, String author, String publisher, String pubDate, String quality, String genre, String desc, String addedNotes, boolean ifRequested) {
        this.userId = userId;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.pubDate = pubDate;
        this.quality = quality;
        this.genre = genre;
        this.desc = desc;
        this.addedNotes = addedNotes;
        this.ifRequested = ifRequested;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) { this.userId = userId; }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String getPubDate() {
        return pubDate;
    }

    public void setPubDate(String pubDate) {
        this.pubDate = pubDate;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getAddedNotes() {
        return addedNotes;
    }

    public void setAddedNotes(String addedNotes) {
        this.addedNotes = addedNotes;
    }

    public boolean getIfRequested() { return ifRequested; }

    public void getIfRequested(boolean ifRequested) { this.ifRequested = ifRequested; }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("userId", userId);
        result.put("title", title);
        result.put("author", author);
        result.put("publisher", publisher);
        result.put("pubDate", pubDate);
        result.put("quality", quality);
        result.put("genre", genre);
        result.put("desc", desc);
        result.put("addedNotes", addedNotes);
        result.put("ifRequested", ifRequested);

        return result;
    }
}
