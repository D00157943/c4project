package pocversion.acaro.com.bookswap;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * Created by acaro on 21/02/2017.
 */

public class Page1AddBookActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    EditText addBookTitle;
    EditText addBookAuthor;
    EditText addBookPublisher;
    EditText addBookPubDate;
    Spinner addBookQualitySpinner;
    ImageButton nextPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_page1_add_book);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        <--  Source: https://developer.android.com/training/implementing-navigation/nav-drawer.html Accessed: February 2017<date>
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
//        -->

        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        addBookQualitySpinner = (Spinner) findViewById(R.id.addBookQualitySpinner);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.quality_spinner_array, android.R.layout.simple_spinner_item);
//// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//// Apply the adapter to the spinner
        addBookQualitySpinner.setAdapter(adapter);

        nextPage = (ImageButton) findViewById(R.id.addBookNextPageImageButton);

        nextPage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                addBookTitle = (EditText) findViewById(R.id.addBookTitleEditText);
                addBookAuthor = (EditText) findViewById(R.id.addBookAuthorEditText);
                addBookPublisher = (EditText) findViewById(R.id.addBookPublisherEditText);
                addBookPubDate = (EditText) findViewById(R.id.addBookPubDateEditText);

                final String bookTitle = addBookTitle.getText().toString().trim();
                final String bookAuthor = addBookAuthor.getText().toString().trim();
                final String bookPublisher = addBookPublisher.getText().toString().trim();
                final String bookPubDate = addBookPubDate.getText().toString().trim();
                final String bookQuality = addBookQualitySpinner.getSelectedItem().toString().trim();

                Intent intent = new Intent(Page1AddBookActivity.this, Page2AddBookActivity.class);
                Bundle data = new Bundle();
                data.putString("bookTitle", bookTitle);
                data.putString("bookAuthor", bookAuthor);
                data.putString("bookPublisher", bookPublisher);
                data.putString("bookPubDate", bookPubDate);
                data.putString("bookQuality", bookQuality);
                intent.putExtra("bookData", data);
                startActivity(intent);
            }
        });

    }

//    <--  Source: https://developer.android.com/training/implementing-navigation/nav-drawer.html Accessed: February 2017<date>
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent;

        if (id == R.id.nav_home) {
            // Handle the camera action
            intent = new Intent(Page1AddBookActivity.this, HomeActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_profile) {
            intent = new Intent(Page1AddBookActivity.this, ProfileActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_messages) {
            intent = new Intent(Page1AddBookActivity.this, MessagesMenuActivity.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
//    -->
}
