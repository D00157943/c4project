package pocversion.acaro.com.bookswap;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by acaro on 03/04/2017.
 */

public class CreatePendingRequest {

    private String title;
    private String ownerID;
    private String bookKey;
    private String requesterID;

    public CreatePendingRequest () {

    }

    public CreatePendingRequest (String title, String ownerID, String bookKey, String requesterID) {
        this.title = title;
        this.ownerID = ownerID;
        this.bookKey = bookKey;
        this.requesterID = requesterID;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(String ownerID) {
        this.ownerID = ownerID;
    }

    public String getBookKey() {
        return bookKey;
    }

    public void setBookKey(String bookKey) {
        this.bookKey = bookKey;
    }

    public String getRequesterID() {
        return requesterID;
    }

    public void setRequesterID(String requesterID) {
        this.requesterID = requesterID;
    }

    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("title", title);
        result.put("ownerID", ownerID);
        result.put("bookKey", bookKey);
        result.put("requesterID", requesterID);

        return result;
    }
}
