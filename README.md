### C4 PROJECT BOOKSWAP ###

* Beta Release

* BookSwap is an Android application that enables users to swap books within their 
* locality.

* Developed in Android Studio using Java.

* Uses Firebase Authentication and Firebase Realtime Database to manage and store information.

* Uses Google Play services to find last known location of user.

##Tutorials for Firebase##

* [Firebase Set Up](https://firebase.google.com/docs/android/setup)
* [Firebase Authentication Set Up](https://firebase.google.com/docs/auth/android/start/)
* [Firebase Auth Manage Accounts](https://firebase.google.com/docs/auth/android/manage-users)
* [Firebase Auth Password Auth](https://firebase.google.com/docs/auth/android/password-auth)
* [Firebase Realtime Database Set Up](https://firebase.google.com/docs/database/android/start/)
* [Firebase Database Read and Write Data](https://firebase.google.com/docs/database/android/read-and-write)
* [Firebase Realtime Database Data Lists](https://firebase.google.com/docs/database/android/lists-of-data)

##Tutorials for Location##
* [Android Last Known Location](https://developer.android.com/training/location/retrieve-current.html)
* [Calculate Distance](https://rosettacode.org/wiki/Haversine_formula#Java )

##Tutorials for Layout##
* [Creating a Navigation Drawer](https://developer.android.com/training/implementing-navigation/nav-drawer.html)
* [Tab View](http://www.truiton.com/2015/06/android-tabs-example-fragments-viewpager/)
* [Tab View](http://www.voidynullness.net/blog/2015/08/16/android-tablayout-design-support-library-tutorial/)